require('dotenv').config()
const linebot = require('@line/bot-sdk')
const rp = require('request-promise')
const app = require('express')()

const lineconf = {
  channelSecret: process.env.LINE_CHANNEL_SECRET,
  channelAccessToken: process.env.LINE_CHANNEL_ACCESS_TOKEN
}

const client = new linebot.Client(lineconf)

const getUserProfileSDK = userid => {
  return new Promise((res, rej) => {
    const client = new linebot.Client({
      channelAccessToken: process.env.LINE_CHANNEL_ACCESS_TOKEN
    })

    client.getProfile(userid)
      .then(profile => { // { userId: '', displayName: '', pictureUrl: '', statusMessage: '' }
        res(profile)
      })
      .catch(e => rej(e))
  })
}

const myEventHandler = async event => {
  const reply = { type: 'text' } // default to text reply

  if (event.message.text && event.message.text.includes('getmyid')) {
    const user = await getUserProfileSDK(event.source.userId)
    reply.text = `Okay ${ user.displayName }, your Line id is ${ user.userId }.`
  } else if (event.type === 'message' && event.message.type === 'text') {
    reply.text = `So you\'re saying "${ event.message.text.trim() }"?`
    // ignore non-text-message event
    // return Promise.resolve(null);
  } else {
    reply.text = `Opps! I have no idea what\'s that ${ event.message.type } you\'ve just sent me.`
  }

  // use reply API
  return client.replyMessage(event.replyToken, reply)
}

app.post('/linewebhook', linebot.middleware(lineconf), (req, res) => {
  Promise
    .all(req.body.events.map(myEventHandler))
    .then(result => res.json(result))
    .catch(e => {
      console.error(e)
      res.status(500).end()
    })
})

app.listen(process.env.PORT_V2, () => {
  console.log(`V2 line bot ready on port ${ process.env.PORT_V2 }..`)
})
