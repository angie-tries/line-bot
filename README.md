## 🤖 Line Bot(s) Test Run
Testout bot development with different packages.


### 🚀 Get Started: clone and install

Yarn or npm, matter of preference.
```
git clone git@git.devs.to:angieyap/line-bot.git && cd line-bot && yarn install
```

### 📦 Packages used:
- V1: [linebot](https://www.npmjs.com/package/linebot)
- V2: [@line/bot-sdk](https://www.npmjs.com/package/@line/bot-sdk)

### 📝 Instructions
1. Start local messaging server (nodemon or node)
```
# V1
nodemon v1-line-bot.js

# or V2
nodemon v2-line-bot-sdk.js
```

2. Publish your localhost endpoint on [ngrok](https://ngrok.com/).
    - Sign in / Register the free account and download the client.
    - Unzip downloaded file and start the server as per instructed.
    - Copy the ngrok address in terminal:
    ![sample-screenshot](assets/ngrok-webhook.png)
    - Go to [Line Developers Console](https://developers.line.biz/console/channel/1653436735/basic/) change your webhook URL to the **copied URL** + `/webhook` --i.e. `9a11565a.ngrok.io/webhook`
    - *Make sure your ngrok server is started on same port as your local messaging server


### 📚 Resources:

**V1**
* https://medium.com/pyradise/%E4%BD%BF%E7%94%A8node-js%E5%BB%BA%E7%BD%AE%E4%BD%A0%E7%9A%84%E7%AC%AC%E4%B8%80%E5%80%8Bline-bot-590b7ba7a28a (It's ~~traditional~~ Chinese. GLHF)

**V2**
* https://line.github.io/line-bot-sdk-nodejs/getting-started/basic-usage.html
