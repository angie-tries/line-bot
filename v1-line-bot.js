require('dotenv').config()
const linebot = require('linebot')
const promisify = require('util').promisify

const bot = linebot({
  channelId: process.env.LINE_CHANNEL_ID,
  channelSecret: process.env.LINE_CHANNEL_SECRET,
  channelAccessToken: process.env.LINE_CHANNEL_ACCESS_TOKEN
})

const getProfile = p => {
  return new Promise((res, rej) => {
    p()
      .then(profile => {
        res(profile)
      })
      .catch(e => {
        console.log('Error gettin\' profile.')
        rej(e)
      })
  })
}

bot.on('message', async event => {
  const user = await getProfile(event.source.profile)
  let msg = `Sorry ${ user.displayName }, i did not understand "${ event.message.text }".`

  event.reply(msg)
    .then(async () => {
      // code here doesn't affect what is being replied
      msg = 'changed'
      console.log(event)
    })
    .catch(e => {
      console.log(e)
    })
})

bot.listen('/linewebhook', process.env.PORT_V1, () => {
  console.log(`Line bot ready on ${ process.env.PORT_V1 }`)
})
